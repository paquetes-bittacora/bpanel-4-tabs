#Tabs

Módulo para generar los tabs de navegación desde cada paquete.

**Nuevo:** ahora también se permiten tabs dinámicos (ver más abajo)

## Uso

### Registrar tabs desde un módulo

Para registrar tabs desde un módulo, se usará la función `Tabs::createItem`, que
recibe los parámetros:

- **key:** El nombre de la ruta donde se quieren mostrar los tabs. Es 
  imprescindible que tenga nombre (por ejemplo `role.index`)
- **ruta**: Ruta a la que enlazará el botón que se está añadiendo
- **permiso**: Permiso que debe tener el usuario para que se le muestre el botón
  (puede ser distinto a la ruta)
- **titulo**: Título del botón
- **icono**: Clase del icono
- **nivel**: Nivel en el que se ubicará el botón. Si no se indica será 1. cada
             nivel se mostrará debajo del anterior en el html. Se añaden clases
             CSS para poder cambiar el estilo de los tabs.
  
**Ejemplo:**

Al ejecutar este código al instalar un módulo (o ejecutar los seeders)

```php
// Rutas normales
Tabs::createItem('role.index', 'role.index', 'role.index', 'Roles','fa fa-list-o');
// Ruta con parámetros
Tabs::createItem('role.index', 'role.create', 'role.create', 'Crear', 'fa fa-plus');
// Tabs por niveles
Tabs::createItem('role.index', 'permission.index', 'permission.index', 'Permisos', 'fa fa-user', 2);
```

Se generarían 2 niveles de tabs. En la primera línea estarían los botones 
"Roles" y "Crear", y en la segunda línea el botón "Permisos". (No tiene mucho
sentido, es solo un ejemplo). 



### Parámetros de las rutas
Hemos hecho este módulo asumiendo que los tabs que generaremos se usarán en 
rutas sin parámetros, o si tienen parámetros, se podrán extraer de la ruta que 
el usuario está cargando. Esto es imprescindible ya que si en los tabs se 
incluyen rutas con parámetros (por ejemplo role/{role}/edit), el paquete
intentará sacar esos parámetros de la ruta que se está cargando. Esto se cumple
también cuando haya varios niveles, donde podremos sacar los ids de los 
contenidos padre, id de categoría, etc, a partir de la ruta actual 
(tipo category/5/post/1/edit).

## Alias:

Si se usa la función `Tabs::createAlias` se copiarán los de la ruta que se 
indique. Por ejemplo, si se registra lo siguiente:

```
Tabs::createAlias('role.edit', 'role.index');
```

Al llamar a la ruta role.edit, se usarían los tabs que se hayan definido para 
'role.create'

El primer parámetro de la función es la ruta en la que se quieren mostrar esos
tabs, y el segundo el nombre de la ruta cuyos tabs queremos copiar.

## Tabs dinámicos

En situaciones en las que es necesario definir tabs de forma dinámica, se puede
usar el componente `dynamic-tabs`:

```html
<x-dynamic-tabs :tabs="$tabs"/>
```

El parámetro `tabs` será un array de arrays, en el que se definirán las distintas
pestañas. Cada array debe tener, al igual que pasa con los tabs normales, los
siguientes parámetros:

```
'permission'
'title'
'route'
'color'
'icon'
'params'
```
