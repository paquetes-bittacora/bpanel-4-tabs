<?php
/**
 * Archivo TEMPORAL para hacer pruebas mientras hago el módulo de Tabs.
 */
return [
    'role.index' => [
        [
            'route' => 'module.index',
            'title' => 'Módulos',
            'icon' => 'fa fa-list-ol',
            'permission' => 'module.index'
        ],
        [
            'route' => 'role.index',
            'title' => 'Roles',
            'icon' => 'fa fa-list-ol',
            'permission' => 'role.index'
        ],
        [
            'route' => 'role.create',
            'title' => 'Nuevo',
            'icon' => 'fa fa-plus',
            'permission' => 'role.create'
        ],
        [
            'route' => 'role.edit',
            'title' => 'Editar',
            'icon' => 'fa fa-pencil',
            'permission' => 'role.edit'
        ],
        [
            'route' => 'role.show',
            'title' => 'Ver',
            'icon' => 'fa fa-eye',
            'permission' => 'role.show'
        ],
    ],
    'role.edit' => [
        'alias' => 'role.index'
    ],
    'role.create' => [
        [
            'route' => 'role.edit',
            'title' => 'Ver',
            'icon' => 'fa fa-eye',
            'permission' => 'role.show'
        ],
    ]
];
