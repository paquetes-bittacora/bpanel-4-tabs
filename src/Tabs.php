<?php

namespace Bittacora\Tabs;

use Bittacora\Tabs\Models\TabItem;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Route;

class Tabs
{
    public static function createAlias(string $key, string $alias)
    {
        $routeItem = TabItem::create([
            'key' => $key,
            'route' => '',
            'permission' => '',
            'title' => '',
            'parent' => null,
            'icon' => null
        ]);
        $routeItem->alias = $alias;
        $routeItem->save();
    }

    public static function createItem(
        string $key,
        string $route,
        string $permission,
        string $title,
        ?string $icon,
        int $level = 1
    ) {
        $routeItem = TabItem::create([
            'key' => $key,
            'route' => $route,
            'permission' => $permission,
            'title' => $title,
            'icon' => $icon,
            'level' => $level
        ]);
        $routeItem->save();
    }

    private static function getTabsByLevel($tabsGroup, $level = 1)
    {
        $tabs = TabItem::where('key', '=', $tabsGroup)->where('level', '=', $level)->get();

        // Comprobamos si es un alias
        if ($tabs->count() === 1 and !empty($tabs[0]->alias)) {
            return self::getTabs($tabs[0]->alias);
        }

        self::addRouteParameters($tabs);


        if ($tabs->count() === 0) {
            return new Collection();
        }

        $tabs->children = self::getTabsByLevel($tabsGroup, ++$level);

        return $tabs;
    }

    /**
     * @param string $tabsGroup Key del grupo de tabs que se quiere obtener
     * @return Collection
     */
    public static function getTabs($tabsGroup): Collection
    {
        return self::getTabsByLevel($tabsGroup);
    }

    private static function addRouteParameters(&$contents)
    {
        // Obtengo los parámetros de la ruta que está intentando cargar el usuario
        $requestedRouteParameters = Route::getCurrentRoute()->parameters;
        foreach ($contents as $content) {
            $route = Route::getRoutes()->getByName($content->route);
            if (!empty($route)) {
                $parameterValues = [];
                $parameters = $route->parameterNames();
                foreach ($parameters as $parameter) {
                    if (isset($requestedRouteParameters[$parameter])) {
                        $parameterValues[$parameter] = $requestedRouteParameters[$parameter];
                    }
                }
                $content->params = $parameterValues;
            }
        }
    }
}
