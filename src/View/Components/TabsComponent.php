<?php

namespace Bittacora\Tabs\View\Components;

use Bittacora\Tabs\Tabs;
use Illuminate\Support\Facades\Route;
use Illuminate\View\Component;

/**
 * Añado "Component" al nombre del componente porque si no daba problemas con el nombre del Facade.
 */
class TabsComponent extends Component
{

    public $tabs; // Uso este nombre porque si no se cogían otras que no sé de donde salen

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($tabsGroup = null)
    {
        $tabsGroup = !empty($tabsGroup) ? $tabsGroup : Route::currentRouteName();
        $this->tabs = Tabs::getTabs($tabsGroup);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('tabs::components.tabs', ['level' => 1]);
    }
}
