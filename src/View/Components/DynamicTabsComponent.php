<?php

namespace Bittacora\Tabs\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

class DynamicTabsComponent extends Component
{
    public function __construct(public readonly array $tabs)
    {
    }

    public function render(): View
    {
        return view('tabs::components.dynamic-tabs', ['level' => 1]);
    }
}
