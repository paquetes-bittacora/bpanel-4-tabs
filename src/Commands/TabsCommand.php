<?php

namespace Bittacora\Tabs\Commands;

use Illuminate\Console\Command;

class TabsCommand extends Command
{
    public $signature = 'tabs';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('All done');
    }
}
