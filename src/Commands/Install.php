<?php

namespace Bittacora\Tabs\Commands;

use Illuminate\Console\Command;

class Install extends Command
{
    protected $signature = 'tabs:install';

    protected $name = 'install';

    protected $description = 'Instala el paquete tabs';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        
    }
}
