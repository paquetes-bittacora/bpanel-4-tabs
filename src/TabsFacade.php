<?php

namespace Bittacora\Tabs;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Tabs\Tabs
 */
class TabsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'tabs';
    }
}
