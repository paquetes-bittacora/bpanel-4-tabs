<?php

namespace Bittacora\Tabs;

use Bittacora\Tabs\Commands\Install;
use Bittacora\Tabs\View\Components\DynamicTabsComponent;
use Bittacora\Tabs\View\Components\TabsComponent;
use Illuminate\Support\Facades\Blade;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\Tabs\Commands\TabsCommand;

class TabsServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('tabs')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_tabs_table')
            ->hasCommand(TabsCommand::class);
    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'tabs');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations/');

        Blade::component('tabs', TabsComponent::class);
        Blade::component('dynamic-tabs', DynamicTabsComponent::class);

        if ($this->app->runningInConsole()) {
            $this->commands([
                Install::class,
            ]);
        }
    }
    
}
