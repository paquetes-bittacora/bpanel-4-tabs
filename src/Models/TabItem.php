<?php

namespace Bittacora\Tabs\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TabItem extends Model
{
    use HasFactory;

    public $fillable = [
        'key',
        'route',
        'permission',
        'title',
        'icon',
        'level',
    ];
}
